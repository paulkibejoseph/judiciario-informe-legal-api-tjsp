import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
// import * as admin from "firebase-admin";
import * as express from "express";
import * as compression from "compression";
import * as cors from "cors";
import * as cookieParser from "cookie-parser";
import * as bodyParser from "body-parser";

admin.initializeApp(functions.config().firebase);
const firestore = admin.firestore();

const api = require("./api/processo");

const app = express();

app.set("json spaces", 4);

app.use(compression());
app.use(cors({ origin: true }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// const tapRequest = (req, res, next) => {
//     console.log('tapRequest', req);
//     return next();
// };

// app.use(tapRequest);

app.use("/api/v1/processo", api);

exports.consultarProcessoTJSP = functions.https.onRequest(app);

exports.gerenciarProcessoTJSP = functions.pubsub
  .topic("gerenciar-processo-tjsp")
  .onPublish(event => {
    const pubSubMessage = event.data;
    const messageBody = pubSubMessage.data
      ? Buffer.from(pubSubMessage.data, "base64").toString()
      : null;
    if (messageBody && messageBody["numProcesso"] && messageBody["processo"]) {
      const numProcesso = messageBody["numProcesso"];
      const processo = messageBody["processo"];
      const processoRef = firestore.collection("processos").doc(numProcesso);
      const setProcesso = processoRef.set({ ...processo }, { merge: true }).then(result => {
        console.log("gerenciarProcessoTJSP", result);
      });
    }
    console.log("gerenciarProcessoTJSP", messageBody);
  });
